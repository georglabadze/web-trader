function setScrollbars () {
	$('.market__list').overlayScrollbars({
		className:       "os-theme-light main-scrollbar",
		paddingAbsolute: true,
		scrollbars:      {
			clickScrolling: true
		}
	});
	$('.trade-table__container').overlayScrollbars({
		className:       "os-theme-light main-scrollbar",
		paddingAbsolute: true,
		scrollbars:      {
			clickScrolling: true
		}
	});
}

function marketDropdown () {
	$('.market__sort-placeholder').click(function () {
		let selector = $(this).closest('.market__sort-dropdown');

		if (selector.hasClass('active')) {
			selector.removeClass('active');
		} else {
			$('.market__sort-placeholder').each(function (index, item) {
				$(item).closest('.market__sort-dropdown').removeClass('active');
			});
			selector.addClass('active');
		}
	});

	$('.market__sort-list span').click(function () {
		$(this).closest('.market__sort-dropdown').find('.market__sort-placeholder').text($(this).text());
		$(this).closest('.market__sort-dropdown').removeClass('active');
	});
}


function markupClickers () {
	$('.market__list-card__favorite').click(function () {
		$(this).toggleClass('active');
	})

	$('.balance__btn').click(function (e) {
		e.preventDefault();
		$(this).closest('.balance').toggleClass('active');
	});
	$('.user__btn').click(function (e) {
		e.preventDefault();
		$(this).closest('.user').toggleClass('active');
	});
	$('.lang__btn').click(function (e) {
		e.preventDefault();
		$(this).closest('.lang').toggleClass('active');
	});
}

function sidebarEvents () {
	let firstItem = $('a[data-show]').eq(0);

	firstItem.addClass('active');

	$('div[data-target="' + firstItem.attr('data-show') + '"]').show();
	$('a[data-show]').click(function (e) {
		e.preventDefault();

		$('.sidebar__list a').removeClass('active');
		$(this).addClass('active');
		$('.trade-section').hide();

		let attr = $(this).attr('data-show');
		$('div[data-target="' + attr + '"]').show();
	})

	$('.history-btn-sm').click(function () {
		$('.trade-nav').toggleClass('back');
	})

	$('.back-btn').click(function () {
		$('div[data-target="history-section"]').hide();
		$('div[data-target="chart-section"]').show();
		$('.trade-nav').removeClass('back');
	})

	$('.add-card-btn, .market-btn, .market-name-sm, .mobile-cls').click(function () {
		$('.trade').toggleClass('active');
	});
}

function tradeCards () {
	$('.trade-nav__card .cls').click(function () {
		$(this).closest('.trade-nav__card').remove();
		hideInfo();
	});

	function hideInfo () {
		let card = $('.trade-nav__card');
		let length = card.length;
		if (length >= 4) {
			card.find('.trade-nav__card-info').hide();
			$('.add-card-btn').hide();
		} else {
			card.find('.trade-nav__card-info').show();
			$('.add-card-btn').show();
		}
	}

	hideInfo();
}

function layoutSwitcher () {
	$('.content-layouts .layout').click(function () {
		let attr = $(this).attr('data-layout');
		let layoutContainer = $('.chart-layout');

		$('.content-layouts .layout').removeClass('active');
		$(this).addClass('active');

		layoutContainer.removeClass('one two four');
		layoutContainer.addClass(attr);

		$('#layoutModal').modal('hide');
	});
}

function hamburger(){
	$('.hamburger').click(function () {
		$(this).find('.menu').toggleClass('active');
		$(this).closest('.trade-nav').find('.mobile-sidebar').toggleClass('active');
	})
}

$(function () {
	marketDropdown();
	setScrollbars();
	markupClickers();
	sidebarEvents();
	tradeCards();
	layoutSwitcher();
	hamburger();
})

//# sourceMappingURL=main.js.map
